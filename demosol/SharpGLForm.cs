﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SharpGL;

namespace demosol
{
    /// <summary>
    /// The main form class.
    /// </summary>
    public partial class SharpGLForm : Form
    {
        public struct _RGBColor
        {
            public byte red { get; set; }
            public byte green { get; set; }
            public byte blue { get; set; }

        }
        /// <summary>
        /// Initializes a new instance of the <see cref="SharpGLForm"/> class.
        /// </summary>
        public SharpGLForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the OpenGLDraw event of the openGLControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RenderEventArgs"/> instance containing the event data.</param>
        private void openGLControl_OpenGLDraw(object sender, RenderEventArgs e)
        {
            //  Get the OpenGL object.
            OpenGL gl = openGLControl.OpenGL;
            _RGBColor color = new _RGBColor();
            color.red = 0;
            color.green = 0;
            color.blue = 0;
            //  Clear the color and depth buffer.
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            _RGBColor temp = new _RGBColor();
            //  Load the identity matrix.
            gl.LoadIdentity();
            temp.blue = 0;
            temp.red = 255;
            temp.green = 0;

            //  Draw a coloured pyramid.
            //gl.Begin(OpenGL.GL_POINT);
            //gl.Color(temp.red,temp.green,temp.blue);
            //gl.Vertex(100, 100);
            //gl.End();
            PutPixel(100, 100, temp);
            PutPixel(100, 101, temp);
            PutPixel(100, 99, temp);
            PutPixel(99, 100, temp);
            PutPixel(99, 101, temp);
            PutPixel(99, 99, temp);
            PutPixel(101, 100, temp);
            PutPixel(101, 99, temp);
            PutPixel(101, 101, temp);
            if (GetPixel(100, 100).red == 255)
            {
                drawPixel();
            }

        }

        void drawPixel()
        {
            OpenGL gl = openGLControl.OpenGL;
            gl.Begin(OpenGL.GL_LINES);
            gl.Color((byte)255, (byte)0, (byte)0);
            gl.Vertex(200, 200);
            gl.Vertex(500, 500);
            gl.End();
        }
        void PutPixel(int x, int y, _RGBColor color)
        {
            byte[] ptr = new byte[3];
            ptr[0] = color.red;
            ptr[1] = color.green;
            ptr[2] = color.blue;
            OpenGL gl = openGLControl.OpenGL;
            gl.RasterPos(x, y);
            gl.DrawPixels(1, 1, OpenGL.GL_RGB, ptr);
            gl.Flush();
        }

        /// <summary>
        /// Handles the OpenGLInitialized event of the openGLControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void openGLControl_OpenGLInitialized(object sender, EventArgs e)
        {
            //  TODO: Initialise OpenGL here.

            //  Get the OpenGL object.
            OpenGL gl = openGLControl.OpenGL;

            //  Set the clear color.
            gl.ClearColor(0, 0, 0, 0);
        }

        /// <summary>
        /// Handles the Resized event of the openGLControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void openGLControl_Resized(object sender, EventArgs e)
        {
            //  TODO: Set the projection matrix here.

            //  Get the OpenGL object.
            OpenGL gl = openGLControl.OpenGL;

            //  Set the projection matrix.
            gl.MatrixMode(OpenGL.GL_PROJECTION);

            //  Load the identity.
            gl.LoadIdentity();

            gl.Ortho2D(0, 784, 0, 561);
            //  Set the modelview matrix.
            gl.MatrixMode(OpenGL.GL_MODELVIEW);
        }

        /// <summary>
        /// The current rotation.
        /// </summary>
        _RGBColor GetPixel(int x, int y)
        {
            OpenGL gl = openGLControl.OpenGL;
            byte[] ptr = new byte[3];
            
            _RGBColor color = new _RGBColor();
            for (int i=y-2; i <= y + 2; i++)
            {

                for (int j = x - 3; j <= x + 2; j++)
                {
                    gl.ReadPixels(j ,  i, 1, 1, OpenGL.GL_RGB, OpenGL.GL_UNSIGNED_BYTE, ptr);
                    color.red = ptr[0];
                    color.green = ptr[1];
                    color.blue = ptr[2];
                    if (color.red == 255)
                        break;
                }
            }
            
            return color;
        }
    }
}
